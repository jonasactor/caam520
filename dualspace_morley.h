/* Type names for our PetscDualSpace implementations */
#define PETSCDUALSPACEMORLEY "morley"

/* New interface for our PetscDualSpace implementations */
PETSC_EXTERN PetscErrorCode PetscDualSpaceMorleyGetSize(PetscDualSpace, PetscInt *);
PETSC_EXTERN PetscErrorCode PetscDualSpaceMorleySetSize(PetscDualSpace, PetscInt);

PETSC_EXTERN PetscErrorCode PetscDualSpaceGetDimension_Morley(PetscDualSpace, PetscInt *);

PETSC_EXTERN PetscErrorCode PetscDualSpaceCreate_Morley(PetscDualSpace);
PETSC_EXTERN PetscErrorCode PetscDualSpaceSetUp_Morley(PetscDualSpace);


/* Private data for our PetscDualSpace implementations */
typedef struct {
  PetscInt size;
  PetscInt* numDof;
} PetscDualSpace_Morley;
