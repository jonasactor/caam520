#include <petsc/private/petscfeimpl.h> /*I "petscfe.h" I*/
#include "dualspace_morley.h"

/*-------------------------- New Interface for our PetscDualSpace Implementations -------------------------------*/

/*@
  PetscDualSpaceMorleyGetSize - Get the size

  Not collective

  Input Parameter:
. sp - The PetscDualSpace

  Output Parameter:
. size - The size

  Level: intermediate

.seealso: PetscDualSpaceMorleySetSize(), PetscDualSpaceCreate()
@*/
PetscErrorCode PetscDualSpaceMorleyGetSize(PetscDualSpace sp, PetscInt *size)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  PetscValidHeaderSpecific(sp, PETSCDUALSPACE_CLASSID, 1);
  PetscValidPointer(size, 2);
  ierr = PetscTryMethod(sp, "PetscDualSpaceMorleyGetTensor_C", (PetscDualSpace, PetscInt *), (sp, size));CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*@
  PetscDualSpaceMorleySetSize - Set the size

  Not collective

  Input Parameters:
+ sp   - The PetscDualSpace
- size - The size

  Level: intermediate

.seealso: PetscDualSpaceMorleyGetSize(), PetscDualSpaceCreate()
@*/
PetscErrorCode PetscDualSpaceMorleySetSize(PetscDualSpace sp, PetscInt size)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  PetscValidHeaderSpecific(sp, PETSCDUALSPACE_CLASSID, 1);
  ierr = PetscTryMethod(sp, "PetscDualSpaceMorleySetSize_C", (PetscDualSpace, PetscInt), (sp, size));CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*-------------------------- Implementation for our PetscDualSpace Implementations -------------------------------*/

static PetscErrorCode PetscDualSpaceMorleyGetSize_Morley(PetscDualSpace sp, PetscInt *size)
{
  PetscDualSpace_Morley *mor = (PetscDualSpace_Morley *) sp->data;

  PetscFunctionBegin;
  *size = mor->size;
  PetscFunctionReturn(0);
}

static PetscErrorCode PetscDualSpaceMorleySetSize_Morley(PetscDualSpace sp, PetscInt size)
{
  PetscDualSpace_Morley *mor = (PetscDualSpace_Morley *) sp->data;

  PetscFunctionBegin;
  mor->size = size;
  PetscFunctionReturn(0);
}

static PetscErrorCode PetscDualSpaceMorleySetNumDof_Morley(PetscDualSpace sp, PetscInt *dim)
{
  PetscDualSpace_Morley *mor = (PetscDualSpace_Morley *) sp->data;
  PetscErrorCode	ierr;

  PetscFunctionBegin;
  ierr = PetscCalloc1(*dim +1,&mor->numDof);CHKERRQ(ierr);

  if (*dim == 3) {
    mor->numDof[0] = 0;
  }
  mor->numDof[*dim-2] = 1;
  mor->numDof[*dim-1] = 1;
  mor->numDof[*dim] = 0;
  
  PetscFunctionReturn(0);
}


PetscErrorCode PetscDualSpaceGetDimension_Morley(PetscDualSpace sp, PetscInt *dim)
{
  PetscInt       n;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMGetDimension(sp->dm, &n);CHKERRQ(ierr);
  switch(n) {
  case 2 :
    *dim = 6;
    break;
  case 3 :
    *dim = 10;
    break;
  default :
    SETERRQ(PetscObjectComm((PetscObject) sp), PETSC_ERR_ARG_OUTOFRANGE, "ImproperDualSpaceSize - Morley: Dimension %D not supported.");
  }
 PetscFunctionReturn(0);
}



PetscErrorCode PetscDualSpaceSetUp_Morley(PetscDualSpace sp)
{
  PetscReal		*qpoints, *qweights;
  PetscInt 		size, depth, dim = 0;
//  PetscInt 		dim = 0;
  PetscErrorCode	ierr;
  PetscInt		f, d;
  PetscReal 		eps = 1.0e-6;

  PetscFunctionBegin;

  if (!(sp->order==2)) {
    SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Morley Elements only are defined for quadratic spaces");
  }

  ierr = DMGetDimension(sp->dm, &dim); CHKERRQ(ierr);

  if (dim == 1 || dim > 3) {
    SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Morley Elements only work for spatial dimensions 2 and 3 in Petsc");
  }
  else {

//  ierr = DMPlexGetDepth(sp->dm, &depth); CHKERRQ(ierr);
  ierr = PetscDualSpaceGetDimension_Morley(sp, &size);CHKERRQ(ierr);
  ierr = PetscMalloc1(size, &sp->functional);CHKERRQ(ierr);
  
//  ierr = PetscDualSpaceMorleySetSize_Morley(sp, size);CHKERRQ(ierr);
  ierr = PetscDualSpaceMorleySetNumDof_Morley(sp, &dim);CHKERRQ(ierr);

  /* DIM = 2 ONLY */
  if (dim == 2) {


    PetscInt	nvertex = 3;
    PetscScalar vertexCoords[6]     = {-1.0, -1.0,  1.0, -1.0,  -1.0, 1.0};
  
    for (f = 0; f < nvertex; ++f) {
      ierr = PetscQuadratureCreate(PETSC_COMM_SELF, &sp->functional[f]);CHKERRQ(ierr);
      ierr = PetscMalloc1(dim, &qpoints);CHKERRQ(ierr);
      ierr = PetscMalloc1(1,   &qweights);CHKERRQ(ierr);
      ierr = PetscQuadratureSetOrder(sp->functional[f], 0);CHKERRQ(ierr);
      ierr = PetscQuadratureSetData(sp->functional[f], dim, 1, 1, qpoints, qweights);CHKERRQ(ierr);
      for (d = 0; d < dim; ++d) {
        qpoints[d] = vertexCoords[dim*f + d];
      }
      qweights[0] = 1.0;
    }

        /* Bottom of Reference Triangle */
    ierr = PetscQuadratureCreate(PETSC_COMM_SELF, &sp->functional[f]);CHKERRQ(ierr);
    ierr = PetscMalloc1(2*dim, &qpoints);CHKERRQ(ierr);
    ierr = PetscMalloc1(2,   &qweights);CHKERRQ(ierr);
    ierr = PetscQuadratureSetOrder(sp->functional[3], 0);CHKERRQ(ierr);
    ierr = PetscQuadratureSetData(sp->functional[3], dim, 1, 2, qpoints, qweights);CHKERRQ(ierr);
    PetscScalar qp[5] = {0.0, -1.0+eps, 0.0, -1.0-eps, 0.0};
    for (d = 0; d < 4; ++d) {
      qpoints[d] = qp[d];
    }
    qweights[0] = -1.0/(2.0*eps);
    qweights[1] = 1.0/(2.0*eps);
    ++f;

        /* Left of Reference Triangle */
    ierr = PetscQuadratureCreate(PETSC_COMM_SELF, &sp->functional[f]);CHKERRQ(ierr);
    ierr = PetscMalloc1(2*dim, &qpoints);CHKERRQ(ierr);
    ierr = PetscMalloc1(2,   &qweights);CHKERRQ(ierr);
    ierr = PetscQuadratureSetOrder(sp->functional[4], 0);CHKERRQ(ierr);
    ierr = PetscQuadratureSetData(sp->functional[4], dim, 1, 2, qpoints, qweights);CHKERRQ(ierr);
    for (d = 0; d < 4; ++d) {
      qpoints[d] = qp[d+1];
    }
    qweights[0] = -1.0/(2.0*eps);
    qweights[1] = 1.0/(2.0*eps);
    ++f;

        /* Diagonal for Reference Triangle */
    ierr = PetscQuadratureCreate(PETSC_COMM_SELF, &sp->functional[f]);CHKERRQ(ierr);
    ierr = PetscMalloc1(4*dim, &qpoints);CHKERRQ(ierr);
    ierr = PetscMalloc1(4,   &qweights);CHKERRQ(ierr);
    ierr = PetscQuadratureSetOrder(sp->functional[5], 0);CHKERRQ(ierr);
    ierr = PetscQuadratureSetData(sp->functional[5], dim, 1, 4, qpoints, qweights);CHKERRQ(ierr);
    PetscScalar qp2[8] = {eps, 0.0, -1.0*eps, 0.0, 0.0, eps, 0.0, -1.0*eps};
    PetscScalar qw[4] = {1.0/(2.0*sqrt(2)*eps), -1.0/(2.0*sqrt(2)*eps), 1.0/(2.0*sqrt(2)*eps), -1.0/(2.0*sqrt(2)*eps)};
    for (d = 0; d < 4; ++d) {
      qpoints[2*d] = qp2[2*d];
      qpoints[2*d+1] = qp2[2*d+1];
      qweights[d] = qw[d];
    }
    ++f;
    }
  else {
    SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Morley Elements have not been implemented yet in 3D!");

        /* Face Dual Elements */

    PetscScalar centroidCoords[12]     = {-1.0/3.0, -1.0/3.0, -1.0, -1.0/3.0, -1.0, -1.0/3.0, -1.0, -1.0/3.0, -1.0/3.0, -1.0/3.0, -1.0/3.0, -1.0/3.0};

    f = 0;

        /* Gradient(centroid) \cdot Normal to Face z = -1 */
    ierr = PetscQuadratureCreate(PETSC_COMM_SELF, &sp->functional[f]);CHKERRQ(ierr);
    ierr = PetscMalloc1(6, &qpoints);CHKERRQ(ierr);
    ierr = PetscMalloc1(2,   &qweights);CHKERRQ(ierr);
    ierr = PetscQuadratureSetOrder(sp->functional[f], 0);CHKERRQ(ierr);
    ierr = PetscQuadratureSetData(sp->functional[f], dim, 1, 2, qpoints, qweights);CHKERRQ(ierr);
    PetscScalar qpZ[6] = {centroidCoords[0],centroidCoords[1],centroidCoords[2]+eps,centroidCoords[0],centroidCoords[1],centroidCoords[2]-eps};
    PetscScalar qwZ[2] = {-1.0/(2.0*eps), 1.0/(2.0*eps)};
    for (d = 0; d < 2; ++d) {
      qpoints[dim*d] = qpZ[dim*d];
      qpoints[dim*d+1] = qpZ[dim*d+1];
      qpoints[dim*d+2] = qpZ[dim*d+2];
      qweights[d] = qwZ[d];
    }
    ++f;

        /* Gradient(centroid) \cdot Normal to Face y = -1 */
    ierr = PetscQuadratureCreate(PETSC_COMM_SELF, &sp->functional[f]);CHKERRQ(ierr);
    ierr = PetscMalloc1(6, &qpoints);CHKERRQ(ierr);
    ierr = PetscMalloc1(2,   &qweights);CHKERRQ(ierr);
    ierr = PetscQuadratureSetOrder(sp->functional[f], 0);CHKERRQ(ierr);
    ierr = PetscQuadratureSetData(sp->functional[f], dim, 1, 2, qpoints, qweights);CHKERRQ(ierr);
    PetscScalar qpY[6] = {centroidCoords[3],centroidCoords[4]+eps,centroidCoords[5],centroidCoords[3],centroidCoords[4]-eps,centroidCoords[5]};
    PetscScalar qwY[2] = {-1.0/(2.0*eps), 1.0/(2.0*eps)};
    for (d = 0; d < 2; ++d) {
      qpoints[dim*d] = qpY[dim*d];
      qpoints[dim*d+1] = qpY[dim*d+1];
      qpoints[dim*d+2] = qpY[dim*d+2];
      qweights[d] = qwY[d];
    }
    ++f;

        /* Gradient(centroid) \cdot Normal to Face x = -1 */
    ierr = PetscQuadratureCreate(PETSC_COMM_SELF, &sp->functional[f]);CHKERRQ(ierr);
    ierr = PetscMalloc1(6, &qpoints);CHKERRQ(ierr);
    ierr = PetscMalloc1(2,   &qweights);CHKERRQ(ierr);
    ierr = PetscQuadratureSetOrder(sp->functional[f], 0);CHKERRQ(ierr);
    ierr = PetscQuadratureSetData(sp->functional[f], dim, 1, 2, qpoints, qweights);CHKERRQ(ierr);
    PetscScalar qpX[6] = {centroidCoords[6]+eps,centroidCoords[7],centroidCoords[8],centroidCoords[6]-eps,centroidCoords[7],centroidCoords[8]};
    PetscScalar qwX[2] = {-1.0/(2.0*eps), 1.0/(2.0*eps)};
    for (d = 0; d < 2; ++d) {
      qpoints[dim*d] = qpX[dim*d];
      qpoints[dim*d+1] = qpX[dim*d+1];
      qpoints[dim*d+2] = qpX[dim*d+2];
      qweights[d] = qwX[d];
    }
    ++f;

        /* Gradient(centroid) \cdot Normal to Diagonal Face */
    ierr = PetscQuadratureCreate(PETSC_COMM_SELF, &sp->functional[f]);CHKERRQ(ierr);
    ierr = PetscMalloc1(18, &qpoints);CHKERRQ(ierr);
    ierr = PetscMalloc1(6,   &qweights);CHKERRQ(ierr);
    ierr = PetscQuadratureSetOrder(sp->functional[f], 0);CHKERRQ(ierr);
    ierr = PetscQuadratureSetData(sp->functional[f], dim, 1, 2, qpoints, qweights);CHKERRQ(ierr);
    PetscScalar qp0[18] = {centroidCoords[9],centroidCoords[10],centroidCoords[11]+eps,centroidCoords[9],centroidCoords[10],centroidCoords[11]-eps,  
                           centroidCoords[9],centroidCoords[10]+eps,centroidCoords[11],centroidCoords[9],centroidCoords[10]-eps,centroidCoords[11], 
                           centroidCoords[9]+eps,centroidCoords[10],centroidCoords[11],centroidCoords[9]-eps,centroidCoords[10],centroidCoords[11] };
    PetscScalar qw0[6] = {1.0/(2.0*sqrt(3)*eps), -1.0/(2.0*sqrt(3)*eps), 1.0/(2.0*sqrt(3)*eps), -1.0/(2.0*sqrt(3)*eps), 1.0/(2.0*sqrt(3)*eps), -1.0/(2.0*sqrt(3)*eps)};
    for (d = 0; d < 6; ++d) {
      qpoints[dim*d] = qp0[dim*d];
      qpoints[dim*d+1] = qp0[dim*d+1];
      qpoints[dim*d+2] = qp0[dim*d+2];
      qweights[d] = qw0[d];
    }
    ++f;

    }
  }
  PetscFunctionReturn(0);
}

PetscErrorCode PetscDualSpaceDestroy_Morley(PetscDualSpace sp)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectComposeFunction((PetscObject) sp, "PetscDualSpaceMorleyGetTensor_C", NULL);CHKERRQ(ierr);
  ierr = PetscObjectComposeFunction((PetscObject) sp, "PetscDualSpaceMorleySetTensor_C", NULL);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode PetscDualSpaceDuplicate_Morley(PetscDualSpace sp, PetscDualSpace *spNew)
{
  PetscInt       order;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscDualSpaceCreate(PetscObjectComm((PetscObject) sp), spNew);CHKERRQ(ierr);
  ierr = PetscDualSpaceSetType(*spNew, PETSCDUALSPACEMORLEY);CHKERRQ(ierr);
  ierr = PetscDualSpaceGetOrder(sp, &order);CHKERRQ(ierr);
  ierr = PetscDualSpaceSetOrder(*spNew, order);CHKERRQ(ierr);
//  ierr = PetscDualSpaceMorleyGetSize(sp, &size);CHKERRQ(ierr);
//  ierr = PetscDualSpaceMorleySetSize(*spNew, size);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode PetscDualSpaceSetFromOptions_Morley(PetscOptionItems *PetscOptionsObject, PetscDualSpace sp)
{
//  PetscInt       size;
//  PetscBool      flg;
  PetscErrorCode ierr;

  PetscFunctionBegin;
//  ierr = PetscDualSpaceMorleyGetSize(sp, &size);CHKERRQ(ierr);
  ierr = PetscOptionsHead(PetscOptionsObject, "PetscDualSpace Morley Options");CHKERRQ(ierr);
//  ierr = PetscOptionsInt("-petscdualspace_mor_size", "Size of element", "PetscDualSpaceMorleySetSize", size, &size, &flg);CHKERRQ(ierr);
//  if (flg) {ierr = PetscDualSpaceMorleySetSize(sp, size);CHKERRQ(ierr);}
  ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


PetscErrorCode PetscDualSpaceGetNumDof_Morley(PetscDualSpace sp, const PetscInt **numDof)
{
  PetscFunctionBegin;
  PetscDualSpace_Morley *mor = (PetscDualSpace_Morley *) sp->data;

  PetscFunctionBegin;
  *numDof = mor->numDof;
  PetscFunctionReturn(0);
}

PetscErrorCode PetscDualSpaceGetHeightSubspace_Morley(PetscDualSpace sp, PetscInt height, PetscDualSpace *bdsp)
{
  PetscFunctionBegin;
  *bdsp = NULL;
  PetscFunctionReturn(0);
}

PetscErrorCode PetscDualSpaceInitialize_Morley(PetscDualSpace sp)
{
  PetscFunctionBegin;
  sp->ops->setfromoptions    = PetscDualSpaceSetFromOptions_Morley;
  sp->ops->setup             = PetscDualSpaceSetUp_Morley;
  sp->ops->view              = NULL;
  sp->ops->destroy           = PetscDualSpaceDestroy_Morley;
  sp->ops->duplicate         = PetscDualSpaceDuplicate_Morley;
  sp->ops->getdimension      = PetscDualSpaceGetDimension_Morley;
  sp->ops->getnumdof         = PetscDualSpaceGetNumDof_Morley;
  sp->ops->getheightsubspace = PetscDualSpaceGetHeightSubspace_Morley;
  sp->ops->getsymmetries     = NULL;
  sp->ops->apply	     = PetscDualSpaceApplyDefault;
  PetscFunctionReturn(0);
}

/*MC
  PETSCDUALSPACEMATT = "mor" - A PetscDualSpace object that just does nothing

  Level: intermediate

.seealso: PetscDualSpaceType, PetscDualSpaceCreate(), PetscDualSpaceSetType()
M*/

PETSC_EXTERN PetscErrorCode PetscDualSpaceCreate_Morley(PetscDualSpace sp)
{
  PetscDualSpace_Morley *mor;
  PetscErrorCode       ierr;

  PetscFunctionBegin;
  PetscValidHeaderSpecific(sp, PETSCDUALSPACE_CLASSID, 1);
  ierr     = PetscNewLog(sp, &mor);CHKERRQ(ierr);
  sp->data = mor;

  mor->size = 0;

  ierr = PetscDualSpaceInitialize_Morley(sp);CHKERRQ(ierr);
  ierr = PetscObjectComposeFunction((PetscObject) sp, "PetscDualSpaceMorleyGetSize_C", PetscDualSpaceMorleyGetSize_Morley);CHKERRQ(ierr);
  ierr = PetscObjectComposeFunction((PetscObject) sp, "PetscDualSpaceMorleySetSize_C", PetscDualSpaceMorleySetSize_Morley);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
